# Sistema NewHeroWay

# Executando a aplicação
1. Na raiz do projeto execute: composer install
2. Execute o comando: docker-compose up -d
3. Execute o comando: docker exec -d newheroway_api-php_1 php artisan migrate:fresh --seed

# Utilização do sistema
- Sistema: http://localhost:8081/hero.php
- API: http://localhost:8081/api

# Executando teste unitário
1. Acesse o container (é necessário executar a aplicação): docker exec -it newheroway_api-php_1 bash
2. Execute o PhpUnit: php vendor/bin/phpunit

## Observações
- Obs: Os teste de Feature (end to end) precisam ser executados dentro do container do PHP pois é necessário a comunicação com o container do Banco de Dados.
- Obs 2: Após os testes é necessário resetar a base: docker exec -d newheroway_api-php_1 php artisan migrate:fresh --seeds

# Encerrando a aplicação
1. Na raiz do projeto execute o comando: docker-compose stop

# Estrutura do projeto

## FrontEnd
1. Foi utilizado HTML com Materialize e Javascript com Jquery.

## Backend
1. Container com nginx configurado para a execução do HTML.
2. Container com Php 7.1.16 e todas as biblitecas necessárias para a execução do projeto
3. Container com Postgres

# Diferenciais do projeto
- Teste unitário de todas as classes e teste Feature para validar o comportamente da api.
- Fluxo separado em Controller, Request (quando necessário para definir as regras e parametros obrigatórios), Business (regras de negócio) e Repositories (para acesso ao banco).
- Coverage para a analise dos testes
- Docker

### Todos os direitos reservados a Vitor Espósito.
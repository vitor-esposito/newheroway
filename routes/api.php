<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::prefix('/type')->group(function () {
	Route::get('/', 'TypeHeroesController@all');
	Route::get('/{id}', 'TypeHeroesController@get');
	Route::post('/', 'TypeHeroesController@store');
	Route::put('/{id}', 'TypeHeroesController@update');
	Route::delete('/{id}', 'TypeHeroesController@delete');
});
Route::prefix('/specialties')->group(function () {
	Route::get('/', 'SpecialtiesController@all');
	Route::get('/{id}', 'SpecialtiesController@get');
	Route::post('/', 'SpecialtiesController@store');
	Route::put('/{id}', 'SpecialtiesController@update');
	Route::delete('/{id}', 'SpecialtiesController@delete');
});
Route::prefix('/hero')->group(function () {
	Route::get('/{id}', 'HeroesController@get');
	Route::get('/', 'HeroesController@all');
	Route::post('/', 'HeroesController@store');
	Route::put('/{id}', 'HeroesController@update');
	Route::delete('/{id}', 'HeroesController@delete');
});

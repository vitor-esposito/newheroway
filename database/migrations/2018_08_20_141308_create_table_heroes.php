<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHeroes extends Migration
{
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_heroes_id');
            $table->string('name', 255);
            $table->integer('life');
            $table->integer('defense');
            $table->integer('damage');
            $table->decimal('speed_atack', 3, 1);
            $table->integer('speed_moviment');
            $table->text('img');
            $table->boolean('active')->default(true);
            $table->timestamp('creation')->useCurrent();
            $table->foreign('type_heroes_id')->references('id')->on('type_heroes');
        });
    }

    public function down()
    {
        Schema::drop('heroes');
    }
}

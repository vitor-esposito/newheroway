<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTypeHeroes extends Migration
{
    public function up()
    {
        Schema::create('type_heroes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 255);
            $table->boolean('active')->default(true);
            $table->timestamp('dt_creation')->useCurrent();
        });
    }

    public function down()
    {
        Schema::drop('type_heroes');
    }
}

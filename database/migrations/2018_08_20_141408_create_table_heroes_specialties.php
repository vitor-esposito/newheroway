<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHeroesSpecialties extends Migration
{
    public function up()
    {
        Schema::create('heroes_specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('heroes_id');
            $table->unsignedInteger('specialties_id');
            $table->foreign('heroes_id')->references('id')->on('heroes');
            $table->foreign('specialties_id')->references('id')->on('specialties');
        });
    }

    public function down()
    {
        Schema::drop('heroes_specialties');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSpecialties extends Migration
{
    public function up()
    {
        Schema::create('specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('specialty', 255);
            $table->boolean('active')->default(true);
            $table->timestamp('dt_creation')->useCurrent();
        });
    }

    public function down()
    {
        Schema::drop('specialties');
    }
}

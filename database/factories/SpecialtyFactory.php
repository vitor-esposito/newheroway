<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Specialty::class, function (Faker $faker) {
    return [
        'specialty' => $faker->name
    ];
});
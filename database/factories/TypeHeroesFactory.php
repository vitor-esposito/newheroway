<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\TypeHeroes::class, function (Faker $faker) {
    return [
        'type' => $faker->name
    ];
});
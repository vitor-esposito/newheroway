<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialtiesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('specialties')->insert([
            'specialty' => 'Magia Branca'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Cura'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Lutador'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Tanker'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Invocação'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Ataque à distância'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Matador de Chefes'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Antitanque'
        ]);

        DB::table('specialties')->insert([
            'specialty' => 'Ataque em área'
        ]);
    }
}

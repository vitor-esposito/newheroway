<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('type_heroes')->insert([
            'type' => 'Mago'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Sacerdote'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Lutador'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Mago'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Arqueiro'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Cavaleiro'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Lutador'
        ]);

        DB::table('type_heroes')->insert([
            'type' => 'Espadachim'
        ]);
    }
}

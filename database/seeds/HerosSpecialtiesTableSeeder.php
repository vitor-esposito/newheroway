<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HerosSpecialtiesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('heroes_specialties')->insert([
            'heroes_id' => 1,
            'specialties_id' => 1
        ]);

        DB::table('heroes_specialties')->insert([
            'heroes_id' => 2,
            'specialties_id' => 1
        ]);

        DB::table('heroes_specialties')->insert([
            'heroes_id' => 2,
            'specialties_id' => 2
        ]);

        DB::table('heroes_specialties')->insert([
            'heroes_id' => 3,
            'specialties_id' => 3
        ]);
    }
}

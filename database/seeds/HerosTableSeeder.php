<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HerosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('heroes')->insert([
            'type_heroes_id' => 1,
            'name' => 'STEVEN',
            'life' => 2900,
            'defense' => 200,
            'damage' => 340,
            'speed_atack' => 1.3,
            'speed_moviment' => 320,
            'img' => 'images/heros/steven/steven.gif'
        ]);

        DB::table('heroes')->insert([
            'type_heroes_id' => 2,
            'name' => 'MONA',
            'life' => 3100,
            'defense' => 200,
            'damage' => 180,
            'speed_atack' => 1.3,
            'speed_moviment' => 330,
            'img' => 'images/heros/mona/mona.gif'
        ]);

        DB::table('heroes')->insert([
            'type_heroes_id' => 3,
            'name' => 'MORGAN',
            'life' => 6000,
            'defense' => 360,
            'damage' => 130,
            'speed_atack' => 1.1,
            'speed_moviment' => 300,
            'img' => 'images/heros/morgan/morgan.gif'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(TypeTableSeeder::class);
        $this->call(SpecialtiesTableSeeder::class);
        $this->call(HerosTableSeeder::class);
        $this->call(HerosSpecialtiesTableSeeder::class);
    }
}

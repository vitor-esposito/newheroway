<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
        <link rel="icon" href="">

        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Raleway:300,400,500,700" rel="stylesheet">

        <!--Import css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/style.css" />

        <title>NewHeroWay</title>
        <!--Let browser know website is optimized for mobile-->
        <meta name="description" content="ERP Vendas">
        <meta name="keywords" content="HTML,CSS,JavaScript,Jquery">
        <meta name="author" content="Vitor Esposito">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css">
            body {
                background: url("images/background.jpg");
                margin-top: 90px;
            }

            .card {
                background-color: #3a1f0b !important;
                border: 15px solid #bb7d31;
                border-radius: 30px;
                position: relative;
            }

            .img-lary {
                width: 200px;
                height: 200px;
                position: absolute;
                top: -100px;
                right: 20px;
            }

            .card .card-action {
                border: 0;
                background: transparent;
            }

            .card p {
                color: #d0b4a0;
            }

            .card .card-title {
                color: #d0b4a0;
                margin-right: 190px;
            }

            .card .card-content {
                padding: 34px 24px;
            }

            .btn {
                background-color: #bb7d31;
            }

            .modal {
                background: url("images/background.jpg");
            }

            .modal-content h4 {
                color: white;
            }

            .card .name-hero {
                margin-bottom: 0 !important;
            }

            .card ul ul {
                display: flex;
                align-items: center;
            }

            .card ul ul li {
                padding-right: 15px;
            }

            .card ul li {
                color: #d0b4a0;
                line-height: 30px;
            }

            .card li span {
                color: #bb7d31;
            }

            .modal input {
                color: #fff;
            }

            .modal input:-webkit-input-placeholder{
                color: #fff;
            }

            .loading::before {
                content: '';
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background: rgba(0,0,0,0.7);
                z-index: 9999;
            }

            .loading img {
                position: fixed;
                transform: translate(-50%, -50%);
                z-index: 9999999999;
                animation-name: loading;
                animation-duration: 2s;
                animation-fill-mode: forwards;
            }

            .loading.out img {
                animation-name: loading-out;
                animation-duration: 2s;
                animation-fill-mode: forwards;
            }

            .card-content img {
                position: absolute;
                top: 30px;
                right: 30px;
            }

            .card-content .btn-deletar {
                background-color: red;
                margin-left: 5px;
            }

            .card-content .btn-deletar:hover {
                background-color: red !important;
            }

            .loading.out::before {
                animation-name: background;
                animation-duration: 2s;
                animation-fill-mode: forwards;
            }

            @keyframes loading {
                from {
                    top: 30%;
                    left: 150%;
                }
                to {
                    top: 50%;
                    left: 50%;
                }
            }

            @keyframes loading-out {
                from {
                    top: 50%;
                    left: 50%;
                }
                to {
                    top: 30%;
                    left: -50%;
                }
            }

            @keyframes background {
                from {
                  background: rgba(0,0,0,0.8);
                  z-index: 99999;
                }
                to {
                  background: transparent;
                  z-index: -99999;
                }
            }
        </style>
	</head>
	<body>
        <div class="loading">
            <img src="images/loading.gif">
        </div>
        <div class="row">
            <div class="col s12">
                <div class="card darken-1">
                    <img class="img-lary" src="images/lary.png">
                    <div class="card-content white-text">
                        <span class="card-title">Olá, meu nome é Lary!<br /> Sou o líder de um grande clã chamado New Way.</span>
                        <p>O clã existe a quase dez anos e com passar do tempo estamos com muitos guerreiros em nosso clã. Com o grande crescimento começamos a enfrentar alguns problemas de organização e controle dos nosso guerreiros. Estamos procurando alguém que possa nos ajudar a criar um sistema que nos permita armazenar os dados de cada um dos nossos guerreiros como tipo, especialidade, e por ai vai, para que possamos nos organizar.</p>
                        <p>É possível cadastrar um novo tipo de herói ou até mesmo uma nova especialidade se houver a necessidade. Com tudo pronto é só ir para tela de cadastro de um novo herói.</p>
                    </div>
                    <div class="card-action">
                        <a class="waves-effect waves-light btn modal-trigger" href="#type">Tipos de Heróis</a>
                        <a class="waves-effect waves-light btn modal-trigger" href="#specialty">Especialidades</a>
                        <a class="waves-effect waves-light btn modal-trigger" href="#hero">Cadastrar novo herói</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="list-hero" class="row">
        </div>
        <!-- Type Structure -->
        <div id="type" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Um novo tipo de herói ira nascer...</h4>
                <form id="type-send">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="type-input" type="text" class="validate">
                            <label for="type-input">Tipo do herói</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Gravar</button>
                </form>
            </div>
        </div>
        <!-- Specialty Structure -->
        <div id="specialty" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Defina uma nova especialidade jamais vista...</h4>
                <form id="specialty-send">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="specialty-input" type="text" class="validate">
                            <label for="specialty-input">Especialidade</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Gravar</button>
                </form>
            </div>
        </div>
        <!-- Hero Structure -->
        <div id="hero" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Herói a caminho...</h4>
                <form id="hero-send" enctype="multipart/form-data">
                    <div class="row">
                        <input id="id-input" type="hidden" value="">
                        <div class="input-field col s6">
                            <input id="name-input" type="text" class="validate">
                            <label for="name-input">Nome</label>
                        </div>
                        <div class="input-field col s6">
                            <select id="select-type">
                                <option value="" disabled selected>Tipo do herói</option>
                            </select>
                        </div>
                        <div class="input-field col s6">
                            <select id="select-specialty" multiple>
                                <option value="" disabled selected>Especialidade</option>
                            </select>
                        </div>
                        <div class="input-field col s6">
                            <input id="life-input" type="number" class="validate">
                            <label for="life-input">Vida</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="defense-input" type="number" class="validate">
                            <label for="defense-input">Defesa</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="damage-input" type="number" class="validate">
                            <label for="damage-input">Dano</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="speed_atack-input" type="text" class="validate">
                            <label for="speed_atack-input">Velocida de ataque</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="speed_moviment-input" type="number" class="validate">
                            <label for="speed_moviment-input">Velocidade de movimento</label>
                        </div>
                        <div id="input-file-hero" class="input-field col s6">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Arquivo</span>
                                    <input type="file" id="img-input">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Gravar</button>
                </form>
            </div>
        </div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
		<script type="text/javascript" src="js/jquery.mask.js"></script>
        <script type="text/javascript">
            $(function(){
                $('.modal').modal();
                $('#speed_atack-input').mask('9.9');

                atualizaSistema();

                $('#hero-send').submit(function(event){
                    $('.loading').show().removeClass('out');

                    event.preventDefault();

                    var reader = new FileReader();
                    reader.readAsDataURL(document.getElementById('img-input').files[0]);

                    if (reader.readyState == 1) {
                        reader.onload = function () {
                            insertHero(reader.result);
                        };
                    }else{
                        insertHero();
                    }
                });

                $('#type-send').submit(function(event){
                    $('.loading').show().removeClass('out');
                    event.preventDefault();

                    ajaxFunction('POST', 'api/type', {type: $('#type-input').val()}, function(response){
                        $('.loading').addClass('out');
                        if (response.status == undefined) {
                            $('#type-input').val('');
                            $('#select-type').append('<option value="'+response.id+'">'+response.type+'</option>');
                            $('select').formSelect();
                        }else{
                            M.toast({html: 'Ocorreu um erro ao tentar cadastrar um novo registro', classes: 'rounded', displayLength: '2000'});
                        }
                        setTimeout(function(){ 
                          $('.loading').hide();
                        }, 3000);
                        $('.modal-overlay').click();
                    });
                });

                $('#specialty-send').submit(function(event){
                    $('.loading').show().removeClass('out');
                    event.preventDefault();

                    ajaxFunction('POST', 'api/specialties', {specialty: $('#specialty-input').val()}, function(response){
                        $('.loading').addClass('out');
                        if (response.status == undefined) {
                            $('#specialty-input').val('');
                            $('#select-specialty').append('<option value="'+response.id+'">'+response.specialty+'</option>');
                            $('select').formSelect();
                        }else{
                            M.toast({html: 'Ocorreu um erro ao tentar cadastrar um novo registro', classes: 'rounded', displayLength: '2000'});
                        }
                        setTimeout(function(){ 
                          $('.loading').hide();
                        }, 3000);
                        $('.modal-overlay').click();
                    });
                });

                $(document).on('click', '.btn-deletar', function() {
                    if (confirm('Deseja realmente deletar esse herói?') == true) {
                        $('.loading').show().removeClass('out');
                        ajaxFunction('DELETE', 'api/hero/'+$(this).data('id'), {}, function(response){
                            listHeros();
                        });
                    }
                });

                $(document).on('click', '.btn-alterar', function() {
                    ajaxFunction('GET', 'api/hero/'+$(this).data('id'), {}, function(response){
                        $('#id-input').val(response.id);
                        $('#name-input').val(response.name);
                        $('#life-input').val(response.life);
                        $('#defense-input').val(response.defense);
                        $('#damage-input').val(response.damage);
                        $('#speed_atack-input').val(response.speed_atack);
                        $('#speed_moviment-input').val(response.speed_moviment);
                        $('#select-type').val(response.type_heroes_id);

                        var allSpecialty = [];
                        $.each(response.specialties, function(i, specialty){
                            allSpecialty.push(specialty.specialty.id);
                        });

                        $('#select-specialty').val(allSpecialty);

                        $('select').formSelect();
                        M.updateTextFields();

                        $('#input-file-hero').hide();

                        $('#hero').modal('open');
                    });
                });
            });

            function insertHero(result = null)
            {
                var name = $('#name-input').val(),
                life = $('#life-input').val(),
                defense = $('#defense-input').val(),
                damage = $('#damage-input').val(),
                speed_atack = $('#speed_atack-input').val(),
                speed_moviment = $('#speed_moviment-input').val(),
                type_heroes_id = $('#select-type :selected').val(),
                img = result,
                id = $('#id-input').val(),
                specialties = $('#select-specialty').val();

                var data = {
                    "name": name,
                    "life": life,
                    "defense": defense,
                    "damage": damage,
                    "speed_atack": speed_atack,
                    "speed_moviment": speed_moviment,
                    "img": img,
                    "type_heroes_id": type_heroes_id,
                    "specialties": specialties
                }

                $('#hero-send').trigger('reset');

                if ($('#id-input').val() == '') {
                    var action = 'POST';
                    var endpoint = 'api/hero';
                }else{
                    var action = 'PUT';
                    var endpoint = 'api/hero/'+id;
                    delete data.img;
                }

                $('#id-input').val('');
                $('#input-file-hero').show();

                ajaxFunction(action, endpoint, data, function(response){
                    listHeros();
                    $('.loading').addClass('out');
                    if (response.status !== undefined) {
                        M.toast({html: 'Ocorreu um erro ao tentar cadastrar um novo registro', classes: 'rounded', displayLength: '2000'});
                    }
                    setTimeout(function(){ 
                      $('.loading').hide();
                    }, 3000);
                    $('.modal-overlay').click();
                });
            }

            function listHeros()
            {
                $('.card-heroes').remove();
                ajaxFunction('GET', 'api/hero', {}, function(response){
                    $.each(response, function(index, data){
                        var allSpecialty = [];

                        $.each(data.specialties, function(i, specialty){
                            allSpecialty.push(' '+specialty.specialty.specialty);
                        });

                       $('#list-hero').append('\
                            <div class="col s6 card-heroes">\
                                <div class="card darken-1">\
                                    <div class="card-content white-text">\
                                        <span class="card-title name-hero">'+data.name+'</span>\
                                        <img src="'+data.img+'" width="70">\
                                        <p>'+data.type.type+'</p>\
                                        <ul>\
                                            <li><span>♦</span> Especialidade: '+allSpecialty.join()+'</li>\
                                            <ul>\
                                                <li><span>♦</span> Vida: '+data.life+'</li>\
                                                <li><span>♦</span> Defesa: '+data.defense+'</li>\
                                                <li><span>♦</span> Dano: '+data.damage+'</li>\
                                            </ul>\
                                            <li><span>♦</span> Velocidade de ataque: '+data.speed_atack+'</li>\
                                            <li><span>♦</span> Velocidade de movimento: '+data.speed_moviment+'</li>\
                                            <li><button class="waves-effect waves-light btn-small btn-alterar" data-id="'+data.id+'">Alterar</button><button class="waves-effect waves-light btn-small btn-deletar" data-id="'+data.id+'">Deletar</button></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>');
                    });
                    $('.loading').addClass('out');
                });
            }
            
            function atualizaSistema()
            {
                $('option').remove()
                ajaxFunction('GET', 'api/type', {}, function(response){
                    $('#select-type').append('<option value="" disabled selected>Tipo do herói</option>');
                    $.each(response, function(index, data){
                        $('#select-type').append('<option value="'+data.id+'">'+data.type+'</option>');
                    });

                    ajaxFunction('GET', 'api/specialties', {}, function(response){
                        $('#select-specialty').append('<option value="" disabled selected>Especialidade</option>');
                        $.each(response, function(index, data){
                            $('#select-specialty').append('<option value="'+data.id+'">'+data.specialty+'</option>');
                        });

                        listHeros();
                        $('select').formSelect();
                    });
                });
            }
        </script>
	</body>
</html>

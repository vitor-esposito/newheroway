function ajaxFunction(type, endPoint, data, callback) {
    var url    = "http://localhost:8081/";
    var header = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    $.ajax({
        url: url+endPoint,
        type: type,
        data: JSON.stringify(data),
        headers: header
    }).done(function(data) {
        callback(data);
    }).fail(function(error) {
        callback(error);
    });
}

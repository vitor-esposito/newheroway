<?php

namespace Tests;

use \Mockery AS M;
use Illuminate\Container\Container;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function getContainer(array $classes)
    {
    	$container = M::mock(Container::class)
			->shouldReceive('getInstance')
			->andReturnSelf();

		foreach ($classes as $class => $mock) {
			$container->shouldReceive('make')->with($class)->andReturn($mock);
		}

		return $container->getMock();
    }

    protected function getContainerSpy()
    {
		return M::spy(Container::class);
    }
}

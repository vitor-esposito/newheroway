<?php

namespace Tests\Feature;

class SpecialtiesControllerTest extends TestCaseFeature
{
	public function testRequiredFields()
	{
		$response = $this->json('POST', '/api/specialties', [], static::$header);

		$message['message'] = [
			'specialty' => [
				'Campo obrigatório'
			]
		];

        $this->assertEquals(422, $response->status());
        $this->assertEquals(json_encode($message), $response->content());
	}

	public function testStore()
	{
		$data = [
			'specialty' => 'Teste'
		];

		$response = $this->json('POST', '/api/specialties', $data, static::$header);

		$store = json_decode($response->content(), true);

        $this->assertEquals(201, $response->status());
        $this->assertEquals($data['specialty'], $store['specialty']);
	}

	public function testUpdateAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('PUT', '/api/specialties/12', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testUpdate()
	{
		$response = $this->json('PUT', '/api/specialties/1', ['specialty' => 'testeee'], static::$header);

		$return = json_decode($response->content(), true);

		$this->assertEquals(200, $response->status());
		$this->assertEquals('testeee', $return['specialty']);
	}

	public function testGetAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('GET', '/api/specialties/12', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testGet()
	{
		$response = $this->json('GET', '/api/specialties/1', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testAll()
	{
		$response = $this->json('GET', '/api/specialties', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testDelete()
	{
		$response = $this->json('DELETE', '/api/specialties/9', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}
}

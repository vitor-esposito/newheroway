<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan as Artisan;

class TestCaseFeature extends TestCase
{
    protected static $header = null;

    public function setUp()
    {
        parent::setUp();

        if (!static::$header) {
            Artisan::call('migrate:rollback');
            Artisan::call('migrate');
            Artisan::call('db:seed');

            factory(\App\Models\Specialty::class)->create();
            factory(\App\Models\TypeHeroes::class)->create();

            static::$header = [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json'
            ];
        }
    }
}

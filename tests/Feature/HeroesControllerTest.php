<?php

namespace Tests\Feature;

class HeroesControllerTest extends TestCaseFeature
{
	public function testRequiredFields()
	{
		$response = $this->json('POST', '/api/hero', [], static::$header);

		$message['message'] = [
			'type_heroes_id' => [
				'Campo obrigatório'
			],
			'name' => [
				'Campo obrigatório'
			],
			'life' => [
				'Campo obrigatório'
			],
			'defense' => [
				'Campo obrigatório'
			],
			'damage' => [
				'Campo obrigatório'
			],
			'speed_atack' => [
				'Campo obrigatório'
			],
			'speed_moviment' => [
				'Campo obrigatório'
			],
			'img' => [
				'Campo obrigatório'
			],
			'specialties' => [
				'Campo obrigatório'
			]
		];

        $this->assertEquals(422, $response->status());
        $this->assertEquals(json_encode($message), $response->content());
	}

	public function testStoreAndNotExistsTypeHero()
	{
		$data = [
			'type_heroes_id' => 12,
			'name' => 'Teste',
			'life' => 200,
			'defense' => 100,
			'damage' => 100,
			'speed_atack' => 10.2,
			'speed_moviment' => 10,
			'img' => '123',
			'specialties' => 1
		];

		$message['message'] = [
			'type_heroes_id' => [
				'Registro informado não existe'
			]
		];

		$response = $this->json('POST', '/api/hero', $data, static::$header);

        $this->assertEquals(422, $response->status());
        $this->assertEquals(json_encode($message), $response->content());
	}

	public function testStoreAndNotExistsSpecialties()
	{
		$data = [
			'type_heroes_id' => 1,
			'name' => 'Teste',
			'life' => 200,
			'defense' => 100,
			'damage' => 100,
			'speed_atack' => 10.2,
			'speed_moviment' => 10,
			'img' => '123',
			'specialties' => 12
		];

		$message['message'] = [
			'specialties' => [
				'Especialidade(s) incorreta(s)'
			]
		];

		$response = $this->json('POST', '/api/hero', $data, static::$header);

        $this->assertEquals(422, $response->status());
        $this->assertEquals(json_encode($message), $response->content());
	}

	public function testStore()
	{
		$data = [
			'type_heroes_id' => 1,
			'name' => 'Teste',
			'life' => 200,
			'defense' => 100,
			'damage' => 100,
			'speed_atack' => 10.2,
			'speed_moviment' => 10,
			'img' => '123',
			'specialties' => 1
		];

		$response = $this->json('POST', '/api/hero', $data, static::$header);

		$store = json_decode($response->content(), true);

        $this->assertEquals(201, $response->status());
        $this->assertEquals(strtoupper($data['name']), $store['name']);
	}

	public function testUpdateAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('PUT', '/api/hero/10', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testUpdate()
	{
		$response = $this->json('PUT', '/api/hero/1', ['name' => 'testeee'], static::$header);

		$return = json_decode($response->content(), true);

		$this->assertEquals(200, $response->status());
		$this->assertEquals('testeee', $return['name']);
	}

	public function testGetAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('GET', '/api/hero/10', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testGet()
	{
		$response = $this->json('GET', '/api/hero/1', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testAll()
	{
		$response = $this->json('GET', '/api/hero', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testDelete()
	{
		$response = $this->json('DELETE', '/api/hero/1', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}
}

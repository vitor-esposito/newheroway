<?php

namespace Tests\Feature;

class TypeHeroesControllerTest extends TestCaseFeature
{
	public function testRequiredFields()
	{
		$response = $this->json('POST', '/api/type', [], static::$header);

		$message['message'] = [
			'type' => [
				'Campo obrigatório'
			]
		];

        $this->assertEquals(422, $response->status());
        $this->assertEquals(json_encode($message), $response->content());
	}

	public function testStore()
	{
		$data = [
			'type' => 'Teste'
		];

		$response = $this->json('POST', '/api/type', $data, static::$header);

		$store = json_decode($response->content(), true);

        $this->assertEquals(201, $response->status());
        $this->assertEquals($data['type'], $store['type']);
	}

	public function testUpdateAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('PUT', '/api/type/12', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testUpdate()
	{
		$response = $this->json('PUT', '/api/type/1', ['type' => 'testeee'], static::$header);

		$return = json_decode($response->content(), true);

		$this->assertEquals(200, $response->status());
		$this->assertEquals('testeee', $return['type']);
	}

	public function testGetAndNotFound()
	{
		$message['message'] = 'Registro não encontrado';

		$response = $this->json('GET', '/api/type/12', [], static::$header);

		$this->assertEquals(404, $response->status());
		$this->assertEquals(json_encode($message), $response->content());
	}

	public function testGet()
	{
		$response = $this->json('GET', '/api/type/1', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testAll()
	{
		$response = $this->json('GET', '/api/type', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}

	public function testDelete()
	{
		$response = $this->json('DELETE', '/api/type/5', [], static::$header);

		$this->assertEquals(200, $response->status());
		$this->assertNotEmpty($response->content());
	}
}

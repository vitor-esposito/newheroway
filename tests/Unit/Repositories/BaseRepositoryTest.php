<?php

namespace App\Repositories;

use \Mockery AS M;
use Tests\TestCase;

use App\Models\Specialty;
use Illuminate\Support\Collection;

class BaseRepositoryTest extends TestCase
{
	/**
	 * @covers \App\Repositories\BaseRepository::__construct
	 */
	public function testCreateAbstractBaseRepository()
	{
		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [new Specialty]);

		$this->assertInstanceOf(BaseRepository::class, $baseRepository);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::get
	 */
	public function testGetAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$get = $baseRepository->get();

		$this->assertInstanceOf(Collection::class, $get);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::first
	 */
	public function testFirstAbstractBaseRepositoryAndReturnNull()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('first')
			->andReturn(null)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$first = $baseRepository->first();

		$this->assertNull($first);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::first
	 */
	public function testFirstAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('first')
			->andReturn(new Specialty)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$first = $baseRepository->first();

		$this->assertInstanceOf(Specialty::class, $first);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::where
	 */
	public function testWhereAbstractBaseRepositoryAndSendSimpleArray()
	{
		$whereCondition = [
			'teste' => 'teste'
		];

		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('where')
			->with('teste', '=', 'teste')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$model = $baseRepository->where($whereCondition);

		$this->assertInstanceOf(BaseRepository::class, $model);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::where
	 */
	public function testWhereAbstractBaseRepositoryWithConditionWhere()
	{
		$whereCondition = [
			'teste' => [
				'<>',
				'teste'
			]
		];

		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('where')
			->with('teste', '<>', 'teste')
			->andReturn((object)['teste' => 'ok'])
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$model = $baseRepository->where($whereCondition);

		$this->assertInstanceOf(BaseRepository::class, $model);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::save
	 */
	public function testSaveAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('save')
			->andReturn(new Specialty)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [new Specialty]);

		$save = $baseRepository->save($specialtyMock);

		$this->assertInstanceOf(Specialty::class, $save);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::find
	 */
	public function testFindAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('find')
			->andReturn(new Specialty)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$save = $baseRepository->find(1);

		$this->assertInstanceOf(Specialty::class, $save);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::whereIn
	 */
	public function testWhereInAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('whereIn')
			->andReturn(new Specialty)
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$whereIn = $baseRepository->whereIn('id', [1]);

		$this->assertInstanceOf(BaseRepository::class, $whereIn);
	}

	/**
	 * @covers \App\Repositories\BaseRepository::delete
	 */
	public function testDeleteAbstractBaseRepository()
	{
		$specialtyMock = M::mock(Specialty::class)
			->shouldReceive('delete')
			->andReturnSelf()
			->getMock();

		$baseRepository = $this->getMockForAbstractClass(BaseRepository::class, [$specialtyMock]);

		$delete = $baseRepository->delete(new Specialty);

		$this->assertEquals(true, $delete);
	}

    public function tearDown()
    {
        M::close();
    }
}

<?php

namespace App\Business;

use Exception;
use \Mockery AS M;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Specialty;
use App\Repositories\SpecialtiesRepositories;

class SpecialtiesBusinessTest extends TestCase
{
	/**
	 * @covers \App\Business\SpecialtiesBusiness::__construct
	 */
	public function testCreateBusiness()
	{
		$business = new SpecialtiesBusiness($this->getContainerSpy());

		$this->assertInstanceOf(SpecialtiesBusiness::class, $business);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::get
	 */
	public function testGetAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->get(1);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::get
	 */
	public function testGet()
	{
		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('find')
			->andReturn(new Specialty)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$get = $business->get(1);

		$this->assertInstanceOf(Specialty::class, $get);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::all
	 */
	public function testAll()
	{
		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$get = $business->all();

		$this->assertInstanceOf(Collection::class, $get);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::save
	 */
	public function testSave()
	{
		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('save')
			->andReturn(new Specialty)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$get = $business->save(['id' => 1]);

		$this->assertInstanceOf(Specialty::class, $get);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::update
	 */
	public function testUpdateAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->update(['id' => 1], 1);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::update
	 */
	public function testUpdate()
	{
		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('find', 'save')
			->andReturn(new Specialty)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$update = $business->update(['id' => 1], 1);

		$this->assertInstanceOf(Specialty::class, $update);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::delete
	 */
	public function testDeleteAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->delete(1);
	}

	/**
	 * @covers \App\Business\SpecialtiesBusiness::delete
	 */
	public function testDelete()
	{
		$specialtiesRepositoriesMock = M::mock(SpecialtiesRepositories::class)
			->shouldReceive('find')
			->andReturn(new Specialty)
			->shouldReceive('delete')
			->andReturn('Registro deletado com sucesso')
			->getMock();

		$business = new SpecialtiesBusiness($this->getContainer([
			SpecialtiesRepositories::class => $specialtiesRepositoriesMock,
		]));

		$delete = $business->delete(1);

		$this->assertEquals('Registro deletado com sucesso', $delete);
	}

    public function tearDown()
    {
        M::close();
    }
}

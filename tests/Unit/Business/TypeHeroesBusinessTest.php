<?php

namespace App\Business;

use Exception;
use \Mockery AS M;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

use App\Models\TypeHeroes;
use App\Repositories\TypeHeroesRepositories;

class TypeHeroesBusinessTest extends TestCase
{
	/**
	 * @covers \App\Business\TypeHeroesBusiness::__construct
	 */
	public function testCreateBusiness()
	{
		$business = new TypeHeroesBusiness($this->getContainerSpy());

		$this->assertInstanceOf(TypeHeroesBusiness::class, $business);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::get
	 */
	public function testGetAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->get(1);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::get
	 */
	public function testGet()
	{
		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('find')
			->andReturn(new TypeHeroes)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$get = $business->get(1);

		$this->assertInstanceOf(TypeHeroes::class, $get);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::all
	 */
	public function testAll()
	{
		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$get = $business->all();

		$this->assertInstanceOf(Collection::class, $get);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::save
	 */
	public function testSave()
	{
		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('save')
			->andReturn(new TypeHeroes)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$get = $business->save(['id' => 1]);

		$this->assertInstanceOf(TypeHeroes::class, $get);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::update
	 */
	public function testUpdateAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->update(['id' => 1], 1);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::update
	 */
	public function testUpdate()
	{
		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('find', 'save')
			->andReturn(new TypeHeroes)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$update = $business->update(['id' => 1], 1);

		$this->assertInstanceOf(TypeHeroes::class, $update);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::delete
	 */
	public function testDeleteAndNotFound()
	{
		$builderSpy = M::spy(Builder::class);

		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$delete = $business->delete(1);
	}

	/**
	 * @covers \App\Business\TypeHeroesBusiness::delete
	 */
	public function testDelete()
	{
		$typeHeroesRepositoriesMock = M::mock(TypeHeroesRepositories::class)
			->shouldReceive('find')
			->andReturn(new TypeHeroes)
			->shouldReceive('delete')
			->andReturn('Registro deletado com sucesso')
			->getMock();

		$business = new TypeHeroesBusiness($this->getContainer([
			TypeHeroesRepositories::class => $typeHeroesRepositoriesMock,
		]));

		$delete = $business->delete(1);

		$this->assertEquals('Registro deletado com sucesso', $delete);
	}

    public function tearDown()
    {
        M::close();
    }
}

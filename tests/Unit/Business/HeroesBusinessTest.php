<?php

namespace App\Business;

use DB;
use Exception;
use \Mockery AS M;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Hero;
use App\Models\HeroesSpecialties;
use App\Repositories\HeroesRepositories;
use App\Repositories\HeroesSpecialtiesRepositories;

class HeroesBusinessTest extends TestCase
{
	/**
	 * @covers \App\Business\HeroesBusiness::__construct
	 */
	public function testCreateBusiness()
	{
		$business = new HeroesBusiness($this->getContainerSpy());

		$this->assertInstanceOf(HeroesBusiness::class, $business);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::get
	 */
	public function testGetAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->get(1);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::get
	 */
	public function testGet()
	{
		$builderMock = M::mock(Builder::class)
			->shouldReceive('with', 'find')
			->andReturn(new Hero)
			->getMock();

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderMock)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
		]));

		$get = $business->get(1);

		$this->assertInstanceOf(Hero::class, $get);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::all
	 */
	public function testAll()
	{
		$builderMock = M::mock(Builder::class)
			->shouldReceive('with', 'get')
			->andReturn(new Collection)
			->getMock();

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderMock)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
		]));

		$get = $business->all();

		$this->assertInstanceOf(Collection::class, $get);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::save
	 */
	public function testSaveAndHasError()
	{
		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('save')
			->andReturnUsing(function() {
				$hero = new Hero;
				$hero->id = 1;

				return $hero;
			})
			->getMock();

		$heroesSpecialtiesRepositoriesMock = M::mock(HeroesSpecialtiesRepositories::class)
			->shouldReceive('save')
			->andReturn(null)
			->getMock();

		DB::shouldReceive('beginTransaction', 'commit', 'rollBack')
			->andReturnSelf()
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
			HeroesSpecialtiesRepositories::class => $heroesSpecialtiesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$get = $business->save(['id' => 1]);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::save
	 */
	public function testSave()
	{
		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('save')
			->andReturnUsing(function() {
				$hero = new Hero;
				$hero->id = 1;

				return $hero;
			})
			->getMock();

		$heroesSpecialtiesRepositoriesMock = M::mock(HeroesSpecialtiesRepositories::class)
			->shouldReceive('save')
			->andReturn(new HeroesSpecialties)
			->getMock();

		DB::shouldReceive('beginTransaction', 'commit', 'rollBack')
			->andReturnSelf()
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
			HeroesSpecialtiesRepositories::class => $heroesSpecialtiesRepositoriesMock,
		]));

		$get = $business->save(['specialties' => 1]);

		$this->assertInstanceOf(Hero::class, $get);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::update
	 */
	public function testUpdateAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->update(['id' => 1], 1);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::update
	 */
	public function testUpdate()
	{
		$builderMock = M::mock(Builder::class)
			->shouldReceive('with', 'find')
			->andReturn(new Hero)
			->getMock();

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderMock)
			->shouldReceive('save')
			->andReturn(new Hero)
			->getMock();

		$heroesSpecialtiesRepositoriesMock = M::mock(HeroesSpecialtiesRepositories::class)
			->shouldReceive('where')
			->andReturnSelf()
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
			HeroesSpecialtiesRepositories::class => $heroesSpecialtiesRepositoriesMock
		]));

		$update = $business->update(['id' => 1], 1);

		$this->assertInstanceOf(Hero::class, $update);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::delete
	 */
	public function testDeleteAndNotFoundRegister()
	{
		$builderSpy = M::spy(Builder::class);

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderSpy)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
		]));

		$this->expectException(Exception::class);

		$business->delete(1);
	}

	/**
	 * @covers \App\Business\HeroesBusiness::delete
	 */
	public function testDelete()
	{
		$builderMock = M::mock(Builder::class)
			->shouldReceive('with', 'find')
			->andReturn(new Hero)
			->getMock();

		$heroesRepositoriesMock = M::mock(HeroesRepositories::class)
			->shouldReceive('with')
			->andReturn($builderMock)
			->shouldReceive('delete')
			->andReturn('Registro deletado com sucesso')
			->getMock();

		$heroesSpecialtiesRepositoriesMock = M::mock(HeroesSpecialtiesRepositories::class)
			->shouldReceive('where')
			->andReturnSelf()
			->shouldReceive('get')
			->andReturn(new Collection)
			->getMock();

		$business = new HeroesBusiness($this->getContainer([
			HeroesRepositories::class => $heroesRepositoriesMock,
			HeroesSpecialtiesRepositories::class => $heroesSpecialtiesRepositoriesMock
		]));

		$delete = $business->delete(1);

		$this->assertEquals('Registro deletado com sucesso', $delete);
	}

    public function tearDown()
    {
        M::close();
    }
}

<?php

namespace App\Http\Requests;

use App;
use App\Http\Validation\VerifySpecialties;
use Illuminate\Foundation\Http\FormRequest;

class HeroesControllerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type_heroes_id' => 'bail|required|exists:type_heroes,id',
            'name'           => 'required',
            'life'           => 'required',
            'defense'        => 'required',
            'damage'         => 'required',
            'speed_atack'    => 'required',
            'speed_moviment' => 'required',
            'img'            => 'required',
            'specialties'    => ['bail', 'required', App::make(VerifySpecialties::class)]
        ];
    }

    public function messages(): array
    {
        return [
            'required' => 'Campo obrigatório',
            'exists'   => 'Registro informado não existe'
        ];
    }
}

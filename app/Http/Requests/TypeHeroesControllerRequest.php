<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TypeHeroesControllerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'required'   => 'Campo obrigatório'
        ];
    }
}

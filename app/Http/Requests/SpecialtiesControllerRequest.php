<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpecialtiesControllerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'specialty' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'required'   => 'Campo obrigatório'
        ];
    }
}

<?php

namespace App\Http\Validation;

use Illuminate\Contracts\Validation\Rule;
use App\Repositories\SpecialtiesRepositories;

class VerifySpecialties implements Rule
{
    private $specialtiesRepositories;

    public function __construct(SpecialtiesRepositories $specialtiesRepositories)
    {
        $this->specialtiesRepositories = $specialtiesRepositories;
    }

    public function passes($attribute, $value): bool
    {
        if ($this->specialtiesRepositories->whereIn('id', (array)$value)->get()->count() !== count((array)$value)) {
            return false;
        }

        return true;
    }

    public function message(): string
    {
        return 'Especialidade(s) incorreta(s)';
    }
}

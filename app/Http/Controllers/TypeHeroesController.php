<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\jsonResponse;
use Illuminate\Routing\Controller;

use App\Business\TypeHeroesBusiness;
use App\Http\Requests\TypeHeroesControllerRequest;

class TypeHeroesController extends Controller
{
	private $typeHeroBusiness;

	public function __construct(TypeHeroesBusiness $typeHeroBusiness)
	{
		$this->typeHeroBusiness = $typeHeroBusiness;
	}

	public function get(int $id): jsonResponse
	{
		return response()->json($this->typeHeroBusiness->get($id));
	}

	public function all(): jsonResponse
	{
		return response()->json($this->typeHeroBusiness->all());
	}

	public function store(TypeHeroesControllerRequest $request): jsonResponse
	{
		return response()->json($this->typeHeroBusiness->save($request->all()), 201);
	}

	public function update(Request $request, $id): jsonResponse
	{
		return response()->json($this->typeHeroBusiness->update($request->all(), $id));
	}

	public function delete($id): jsonResponse
	{
		return response()->json(['message' => $this->typeHeroBusiness->delete($id)]);
	}
}

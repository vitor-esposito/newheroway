<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\jsonResponse;
use Illuminate\Routing\Controller;

use App\Business\HeroesBusiness;
use App\Http\Requests\HeroesControllerRequest;

class HeroesController extends Controller
{
	private $heroesBusiness;

	public function __construct(HeroesBusiness $heroesBusiness)
	{
		$this->heroesBusiness = $heroesBusiness;
	}

	public function get(int $id): jsonResponse
	{
		return response()->json($this->heroesBusiness->get($id));
	}

	public function all(): jsonResponse
	{
		return response()->json($this->heroesBusiness->all());
	}

	public function store(HeroesControllerRequest $request): jsonResponse
	{
		return response()->json($this->heroesBusiness->save($request->all()), 201);
	}

	public function update(Request $request, $id): jsonResponse
	{
		return response()->json($this->heroesBusiness->update($request->all(), $id));
	}

	public function delete($id): jsonResponse
	{
		return response()->json(['message' => $this->heroesBusiness->delete($id)]);
	}
}

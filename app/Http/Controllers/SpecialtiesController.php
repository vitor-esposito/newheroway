<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\jsonResponse;
use Illuminate\Routing\Controller;

use App\Business\SpecialtiesBusiness;
use App\Http\Requests\SpecialtiesControllerRequest;

class SpecialtiesController extends Controller
{
	private $specialtiesBusiness;

	public function __construct(SpecialtiesBusiness $specialtiesBusiness)
	{
		$this->specialtiesBusiness = $specialtiesBusiness;
	}

	public function get(int $id): jsonResponse
	{
		return response()->json($this->specialtiesBusiness->get($id));
	}

	public function all(): jsonResponse
	{
		return response()->json($this->specialtiesBusiness->all());
	}

	public function store(SpecialtiesControllerRequest $request): jsonResponse
	{
		return response()->json($this->specialtiesBusiness->save($request->all()), 201);
	}

	public function update(Request $request, $id): jsonResponse
	{
		return response()->json($this->specialtiesBusiness->update($request->all(), $id));
	}

	public function delete($id): jsonResponse
	{
		return response()->json(['message' => $this->specialtiesBusiness->delete($id)]);
	}
}

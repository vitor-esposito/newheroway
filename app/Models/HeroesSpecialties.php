<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HeroesSpecialties extends Model
{
	public $timestamps = false;
	protected $table   = 'heroes_specialties';

	protected $fillable = [
		'id',
		'heroes_id',
		'specialties_id'
	];

	public function specialty()
	{
		return $this->belongsTo('App\Models\Specialty', 'specialties_id');
	}
}

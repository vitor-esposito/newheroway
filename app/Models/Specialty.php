<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'id',
		'specialty',
		'active',
		'dt_creation'
	];
}

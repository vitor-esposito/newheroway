<?php

namespace App\Models;

use App\Events\HeroCreating;
use Illuminate\Database\Eloquent\Model;

class Hero extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'id',
		'type_heroes_id',
		'name',
		'life',
		'defense',
		'damage',
		'speed_atack',
		'speed_moviment',
		'img',
		'active',
		'creation'
	];

	protected $dispatchesEvents = [
		'creating' => HeroCreating::class
	];

	public function specialties()
	{
		return $this->hasMany('App\Models\HeroesSpecialties', 'heroes_id');
	}

	public function type()
	{
		return $this->belongsTo('App\Models\TypeHeroes', 'type_heroes_id');
	}
}

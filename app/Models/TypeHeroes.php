<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeHeroes extends Model
{
	public $timestamps = false;
	protected $table = 'type_heroes';

	protected $fillable = [
		'id',
		'type',
		'active',
		'dt_creation'
	];
}

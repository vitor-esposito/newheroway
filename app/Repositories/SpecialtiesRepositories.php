<?php

namespace App\Repositories;

use App\Models\Specialty;

class SpecialtiesRepositories extends BaseRepository
{
	public function __construct(Specialty $model)
	{
		$this->model = $model;
	}
}
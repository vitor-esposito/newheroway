<?php

namespace App\Repositories;

use App\Models\HeroesSpecialties;

class HeroesSpecialtiesRepositories extends BaseRepository
{
	public function __construct(HeroesSpecialties $model)
	{
		$this->model = $model;
	}
}
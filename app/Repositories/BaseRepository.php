<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

abstract class BaseRepository
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function get(array $columns = ['*']): Collection
    {
    	return $this->model->get($columns);
    }

    public function with(array $tables): Builder
    {
        return $this->model->with($tables);
    }

    public function first(array $columns = ['*']): ?Model
    {
    	return $this->model->first($columns);
    }

    public function find(int $id): ?Model
    {
        return $this->model->find($id);
    }

    public function where(array $where): BaseRepository
    {
		foreach ($where as $field => $value) {
    		list($operation, $val) = (is_array($value)) ? $value : ['=', $value];
    		$this->model = $this->model->where($field, $operation, $val);
    	}

    	return $this;
    }

    public function whereIn(string $field, array $where): BaseRepository
    {
        $this->model = $this->model->whereIn($field, $where);

        return $this;
    }

    public function delete(Model $model): bool
    {
        $model->delete();
        return true;
    }

    public function save(Model $model): Model
    {
        $model->save();
        return $model;
    }
}

<?php

namespace App\Repositories;

use App\Models\Hero;

class HeroesRepositories extends BaseRepository
{
	public function __construct(Hero $model)
	{
		$this->model = $model;
	}
}
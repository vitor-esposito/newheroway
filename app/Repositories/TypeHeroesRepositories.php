<?php

namespace App\Repositories;

use App\Models\TypeHeroes;

class TypeHeroesRepositories extends BaseRepository
{
	public function __construct(TypeHeroes $model)
	{
		$this->model = $model;
	}
}
<?php

namespace App\Business;

use Illuminate\Support\Collection;
use Illuminate\Container\Container;

use App\Models\Specialty;
use App\Repositories\SpecialtiesRepositories;

class SpecialtiesBusiness
{
	private $container;

	public function __construct(Container $container)
	{
		$this->container = $container::getInstance();
	}

	public function get(int $id): Specialty
	{
		$specialtiesRepositories = $this->container->make(SpecialtiesRepositories::class);

		$specialtiesModel = $specialtiesRepositories->find($id);
		if (!$specialtiesModel) {
			throw new \Exception('Registro não encontrado', 404);
		}

		return $specialtiesModel;
	}

	public function all(): Collection
	{
		
		return $this->container->make(SpecialtiesRepositories::class)->get();
	}

	public function save(array $data): Specialty
	{
		return $this->container->make(SpecialtiesRepositories::class)->save(new Specialty($data));
	}

	public function update(array $data, $id): Specialty
	{
		$specialtiesModel = $this->get($id)->fill($data);

		return $this->container->make(SpecialtiesRepositories::class)->save($specialtiesModel);
	}

	public function delete($id): string
	{
		$specialtiesModel = $this->get($id);

		$this->container->make(SpecialtiesRepositories::class)->delete($specialtiesModel);

		return 'Registro deletado com sucesso';
	}
}

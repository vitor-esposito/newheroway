<?php

namespace App\Business;

use Illuminate\Support\Collection;
use Illuminate\Container\Container;

use App\Models\TypeHeroes;
use App\Repositories\TypeHeroesRepositories;

class TypeHeroesBusiness
{
	private $container;

	public function __construct(Container $container)
	{
		$this->container = $container::getInstance();
	}

	public function get(int $id): TypeHeroes
	{
		$typeHeroRepositories = $this->container->make(TypeHeroesRepositories::class);

		$typeHeroModel = $typeHeroRepositories->find($id);
		if (!$typeHeroModel) {
			throw new \Exception('Registro não encontrado', 404);
		}

		return $typeHeroModel;
	}

	public function all(): Collection
	{
		
		return $this->container->make(TypeHeroesRepositories::class)->get();
	}

	public function save(array $data): TypeHeroes
	{
		return $this->container->make(TypeHeroesRepositories::class)->save(new TypeHeroes($data));
	}

	public function update(array $data, $id): TypeHeroes
	{
		$typeHeroModel = $this->get($id)->fill($data);

		return $this->container->make(TypeHeroesRepositories::class)->save($typeHeroModel);
	}

	public function delete($id): string
	{
		$typeHeroModel = $this->get($id);

		$this->container->make(TypeHeroesRepositories::class)->delete($typeHeroModel);

		return 'Registro deletado com sucesso';
	}
}

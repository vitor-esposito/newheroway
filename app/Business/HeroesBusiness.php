<?php

namespace App\Business;

use Illuminate\Support\Collection;
use Illuminate\Container\Container;

use DB;
use Storage;
use App\Models\Hero;
use App\Models\HeroesSpecialties;
use App\Repositories\HeroesRepositories;
use App\Repositories\HeroesSpecialtiesRepositories;

class HeroesBusiness
{
	private $container;

	public function __construct(Container $container)
	{
		$this->container = $container::getInstance();
	}

	public function get(int $id): Hero
	{
		$heroesRepositories = $this->container->make(HeroesRepositories::class);

		$heroesModel = $heroesRepositories->with(['specialties.specialty', 'type'])->find($id);
		if (!$heroesModel) {
			throw new \Exception('Registro não encontrado', 404);
		}

		return $heroesModel;
	}

	public function all(): Collection
	{
		
		return $this->container->make(HeroesRepositories::class)->with(['specialties.specialty', 'type'])->get();
	}

	public function save(array $data): Hero
	{
		try {
			DB::beginTransaction();

			//Insere um novo Herói
			$heroModel = $this->container->make(HeroesRepositories::class)->save(new Hero($data));

			//Instancia o repositório que vai gravar o relacionamento do herói com a especialidade
			$heroesSpecialtiesRepositories = $this->container->make(HeroesSpecialtiesRepositories::class);

			//Cria o relacionamento
			$this->createRelationShip($data, $heroModel->id);

			DB::commit();

			return $heroModel;
		} catch (\Exception $e) {
			DB::rollBack();
			throw new \Exception('Erro ao tentar cadastrar um novo herói', 422);
		}
	}

	public function update(array $data, $id): Hero
	{
		$heroesModel = $this->get($id)->fill($data);

		$heroesSpecialtiesRepositories = $this->container->make(HeroesSpecialtiesRepositories::class);

		//Deleta todos as especialidades
		$heroesSpecialtiesModel = $heroesSpecialtiesRepositories->where(['heroes_id' => $id])->get();
		foreach ($heroesSpecialtiesModel as $model) {
			$heroesSpecialtiesRepositories->delete($model);
		}

		//Cria o relacionamento do herói com as especialidades
		if (isset($data['specialties'])) {
			$this->createRelationShip($data, $id);
		}

		return $this->container->make(HeroesRepositories::class)->save($heroesModel);
	}

	public function delete($id): string
	{
		$heroesSpecialtiesRepositories = $this->container->make(HeroesSpecialtiesRepositories::class);

		$heroesModel = $this->get($id);
		$heroesSpecialtiesModel = $heroesSpecialtiesRepositories->where(['heroes_id' => $id])->get();

		foreach ($heroesSpecialtiesModel as $model) {
			$heroesSpecialtiesRepositories->delete($model);
		}

		$this->container->make(HeroesRepositories::class)->delete($heroesModel);

		return 'Registro deletado com sucesso';
	}

	private function createRelationShip(array $data, int $id): bool
	{
		$heroesSpecialtiesRepositories = $this->container->make(HeroesSpecialtiesRepositories::class);

		foreach ((array)$data['specialties'] as $specialty) {
			$heroesSpecialtiesModel = new HeroesSpecialties;
			$heroesSpecialtiesModel->heroes_id      = $id;
			$heroesSpecialtiesModel->specialties_id = $specialty;

			//Se algum registro falhar, o mesmo retorna uma Exception
			if (!$heroesSpecialtiesRepositories->save($heroesSpecialtiesModel)) {
				throw new \Exception('Erro ao tentar cadastrar um novo herói', 422);
			}
		}

		return true;
	}
}

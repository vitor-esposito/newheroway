<?php

namespace App\Events;

use App\Models\Hero;

class HeroCreating
{
	public function __construct(Hero $hero)
    {
    	$hero->name = strtoupper($hero->name);
    }
}
